"""
Store example.

Covers:

- Waiting for other processes:
    Table
        Jeans
        Shirt
    Fitting Room
- Resources:
    Release following yield
    Release following condition

Scenario:
  A store has a limited number of changing rooms and cash registers.

Customers arrive at the store at a random time. If one of the two changing rooms is available,
they start the changing process; if not, they have to wait for it.

Updated on 29 August 2015

"""

import simpy
import random
import matplotlib.pyplot as plt

RANDOM_SEED = 42
NUM_ROOMS = 2  # Number of fitting rooms in the store
CHANGETIME = 10  # Minutes it takes to try on clothes
T_INTER = 3  # Create a customer every 3 minutes
SIM_TIME = 100  # Simulation time in minutes
PAYTIME = 2  # Time it takes for a customer to pay
NUM_JEANS = 10  # Defines filter resource capacity
NUM_SHIRTS = 10  # Defines filter resource capacity

"""
Data monitoring fuctions
"""
data = []


class Shirt(object):
    def __init__(self, env, shirt_id):
        self.env = env
        self.product = 'shirt'
        self.age = 24
        self.id = shirt_id


class Jeans(object):
    def __init__(self, env, jeans_id):
        self.env = env
        self.product = 'jeans'
        self.id = jeans_id


class LeviStore(object):
    """
    A store has a limited number of fitting rooms (``NUM_Rooms``) to
    allow customers to try on clothes in parallel.

    Customers have to wait for one of the fitting rooms if they are all occupied. When one is free then
    they can start the changing processes and wait for it to finish (which takes ``CHANGETIME`` minutes).
    """

    def __init__(self, env, num_rooms, chgtime, num_registers, num_jeans, num_shirts):
        self.env = env
        self.fittingRoom = simpy.Resource(env, num_rooms)
        self.cashRegister = simpy.Resource(env, num_registers)
        self.changeTime = chgtime
        self.table = Table(env, num_jeans, num_shirts)

    def change(self, customer):
        yield self.env.timeout(CHANGETIME)
        # print("It took %d minutes for %s to change" %  (random.randint(4, 7), customer))

    def pay(self, customer):
        yield self.env.timeout(PAYTIME)
        stop_time = env.now
        total_shop_time = stop_time - customer.start_time
        # print("It took %d minutes for %s to pay" % (random.randint(4, 7), customer))
        shop_data[customer.id] = [customer.id, total_shop_time]


class Table(object):
    def __init__(self, env, num_jeans, num_shirts):
        self.env = env
        self.jeans_resource_store = simpy.FilterStore(env, capacity=num_jeans)
        self.shirts_resource_store = simpy.FilterStore(env, capacity=num_shirts)


class Employee(object):
    def __init__(self, env):
        self.env = env

    def stock_table(self, env, table, num_jeans, num_shirts):
        for i in range(1, num_jeans+1):
            jeans_item = Jeans(env, i)
            yield table.jeans_resource_store.put(jeans_item)
            print('Stocked jeans %d on table at %.2f' % (i, env.now))
        for i in range(1, num_shirts+1):
            shirt_item = Shirt(env, i)
            yield table.shirts_resource_store.put(shirt_item)
            print('Stocked shirts %d on table at %.2f' % (i, env.now))


class Customer(object):
    def __init__(self, env, id_num, store):
        self.env = env
        self.id = id_num
        self.start_time = env.now
        self.stop_time = env.now
        # self.priority = random.randint(50, 100)
        self.clothe_preference = random.randint(0, 100)
        self.wants_fitting_room = bool(random.randint(0, 100) < 50) # Customer wants to go to fitting room if True
        self.wants_to_buy = bool(random.randint(0, 100) < 80)  # Randomly generate a true or false
        self.process = env.process(self.shop(store))
        self.wait_willingness = 3 # random.randint(3,5) # How long user is willing to wait

    def shop(self, s):
        print('%s arrives at the store at %.2f and begins to consider clothes' % (self.id, env.now))

        """
        If a Resource is used with the  "with" statement the resource is automatically released.
        If you call request() without "with", you are responsible to call release() once you are done using the resource
        """
        yield env.timeout(1) # consider for 1 minute
        if self.clothe_preference < 50 and self.wants_to_buy:  # clothe_preference is jeans
            item = yield s.table.jeans_resource_store.get(lambda item: item.product == 'jeans')
            print('%d grabbed jeans %d' % (self.id, item.id))
        elif self.clothe_preference >= 50 and self.wants_to_buy:  # clothe_preference is shirt
            item = yield s.table.shirts_resource_store.get(lambda item: item.product == 'shirt')
            print('%d grabbed shirt %d' % (self.id, item.id))
        else:
            print('Customer %s does not like anything, leaves' % (self.id))
            self.env.exit()

        if self.wants_fitting_room:  # Customer decides to go to fitting room
            print('%s decides to go to fitting room at %.2f' % (self.id, env.now))
            with s.fittingRoom.request() as request:
                result = yield request | env.timeout(self.wait_willingness)
                if request in result:
                    yield env.process(self.go_to_fitting_room(s, item))
                else:
                    print('Took to long to get a fitting room... customer %s decides to leave.' % (self.id))
                    yield env.process(self.put_clothes_back(s, item))
                    self.env.exit()

        if self.wants_to_buy is False:  # User does not want to buy clothes
            yield env.process(self.put_clothes_back(s, item))
            self.env.exit
        else:
            with s.cashRegister.request() as request:
                yield request
                yield env.process(s.pay(self))
                if self.clothe_preference < 50:
                    print('>>>>>>> %s completes the purchase of jeans %d at %.2f. <<<<<<<'
                          % (self.id, item.id, env.now))
                else:
                    print('>>>>>>> %s completes the purchase of shirt %d at %.2f. <<<<<<<'
                          % (self.id, item.id, env.now))
            self.env.exit()

    def put_clothes_back(self, s, item):
        if self.clothe_preference < 50: # Jeans
            print('Customer %s goes and puts jeans %d back on table yielding jean resource at %.2f'
                  % (self.id, item.id, env.now))
            yield s.table.jeans_resource_store.put(item)
        else: # Shirts
            print('Customer %s goes and puts shirt %d back on table yielding shirt resource at %.2f'
                  % (self.id, item.id, env.now))
            yield s.table.shirts_resource_store.put(item)

    def go_to_fitting_room(self, s, item):
        print('%s enters the fitting room at %.2f.' % (self.id, env.now))
        yield env.process(s.change(self.id))

        print('%s leaves the fitting room at %.2f' % (self.id, env.now))
        if self.wants_to_buy:
            if self.clothe_preference < 50:
                print('%s likes the jeans, goes to register at %.2f' % (self.id, env.now))
            else:
                print('%s likes the shirt, goes to register at %.2f' % (self.id, env.now))

def setup(env, num_rooms, chgtime, t_inter, paytime, num_jeans, num_shirts):
    store = LeviStore(env, num_rooms, chgtime, paytime, num_jeans, num_shirts)
    employee1 = Employee(env)
    env.process(employee1.stock_table(env, store.table, num_shirts, num_shirts))  # MUST run stocking method as a process
    # Create 4 initial customers
    yield env.timeout(1) # Wait for doors to open
    for acc in range(1, 10):
        c = Customer(env, acc, store)
    # Create more customers while the simulation is running
    while True:
        yield env.timeout(t_inter)
        acc += 1
        c = Customer(env, acc, store)


# Setup and start the simulation
# random.seed(RANDOM_SEED)  # This helps reproducing the results
shop_data = {}
env = simpy.Environment()
env.process(setup(env, NUM_ROOMS, CHANGETIME, T_INTER, PAYTIME, NUM_JEANS, NUM_SHIRTS))
env.run(until=SIM_TIME)

x = []
y = []
for key in shop_data:
    x.append(shop_data[key][0])
    y.append(shop_data[key][1])
plt.plot(x, y, 'ro')  # make scatter plot
plt.xlabel('customer ID')
plt.ylabel('Time in Store')
plt.title('Customer time in store')
plt.show()
