"""
Store example.

Covers:

- Waiting for other processes
- Resources: Resource

Scenario:
  A store has a limited number of changing rooms and cash registers.

Customers arrive at the store at a random time. If one of the two changing rooms is available, 
they start the changing process; if not, they have to wait for it.

"""

import random
import simpy


RANDOM_SEED = 42
NUM_ROOMS = 2     # Number of fitting rooms in the store
CHANGETIME = 5    # Minutes it takes to try on clothes
T_INTER = 7       # Create a customer every ~7 minutes
SIM_TIME = 60     # Simulation time in minutes
PAYTIME = 8
NUMCUSTS = 10   


class Store(object):
    """A store has a limited number of fitting rooms (``NUM_Rooms``) to
    allow customers to try on clothes in parallel.

    Customers have to wait for one of the fitting rooms if they are all occupied. When one is free then
    they can start the changing processes and wait for it to finish (which takes ``CHANGETIME`` minutes).
    """
    def __init__(self, env, num_rooms, chgtime, num_registers):
        self.env = env
        self.fittingRoom = simpy.PriorityResource(env, num_rooms) # A PriorityResource reorders the queue if processes are waiting for it.
        self.cashRegister = simpy.Resource(env, num_registers) # Registers are first come, first served.
        self.changeTime = chgtime
        self.table = Table37(env)

    def change(self, customer):
        #vThe changing process. It takes a customer who needs a given amount of time to change. 
        yield self.env.timeout(CHANGETIME)
        #print("It took %d minutes for %s to chTnge" %  (random.randint(4, 7), customer))


    def pay(self, customer):
        #vThe changing process. It takes a customer who needs a given amount of time to change. 
        yield self.env.timeout(PAYTIME)
        stopTime = customer.env.now
        totalShopTime = stopTime - customer.startTime
        #print("%s shopped for %d minutes." %  (customer.custName, totalShopTime))
        
        # The customer has completed shopping so capture the data for output. 
        shopData[customer.custName] = [customer.custNo, customer.priority, totalShopTime]


class Customer(object):
    
    def __init__(self, env, custName, i):
        self.env = env
        self.custName = custName
        self.priority = random.randint(0, 100)
        self.custNo = i
        self.startTime = self.env.now
        self.stopTime = self.env.now
        
    def shop(self, s):
        """Each customer has a name, goes to the store, and needs a fitting room.
           Assume customers need a number to enter a fitting room unless it is already free...that's the purpose the priority 
           Once the fitting is complete, the customer goes to pay at a cash register."""
           
        """If a Resource is used with the  "with" statement the resource is automatically released. 
        If you call request() without "with", you are responsible to call release() once you are done using the resource.
        """
        
        with s.table.wjeans.request() as request:
            yield request
            print('%s begins to consider jeans %.2f.' % (self.custName, env.now))
            yield env.process(s.table.consider(self.custName))
        
        
        with s.fittingRoom.request(priority = self.priority) as request:
            yield request
    
            #print('%s with a priority of %i enters the fitting room at %.2f.' % (self.custName, self.priority, env.now))
            yield env.process(s.change(self.custName))
    
            #print('%s leaves the fitting room at %.2f.' % (self.custName, env.now))
        
        with s.cashRegister.request() as request:
            yield request
            
            #print('%s arrives at the cash register at %.2f.' % (self.custName, env.now))
            yield env.process(s.pay(self))
            
            #print('%s completes the purchase at %.2f.' % (self.custName, env.now))
    
class Table37(object):
    def __init__(self, env):
        self.env = env
        self.wjeans = simpy.Resource(env, 3) #3 pairs of women's jeans
    def consider(self, customer):
        yield self.env.timeout(1)
        #print("It took %d minutes for %s to consider jeans" %  (1, customer))



def setup(env, num_rooms, chgtime, t_inter, paytime, numCusts):
    """Create a Store, a number of initial customers and keep creating customers
    approximately every ``t_inter`` minutes."""
    # Create the Store
    store = Store(env, num_rooms, chgtime, paytime)
    
    # Create 4 initial customers
    for i in range(numCusts):
        c = Customer(env, "Customer %d " % i, i)
        env.process(c.shop(store))
    
    print(c.custName, c.custNo)
    
    # Create more customers while the simulation is running
    while True:
        c = Customer(env, "Customer %d " % numCusts, numCusts)
        #yield env.timeout(random.randint(t_inter-2, t_inter+2))
        yield env.timeout(5)
        env.process(c.shop(store))
        numCusts += 1
        
# Setup and start the simulation
random.seed(RANDOM_SEED)  # This helps reproducing the results

shopData = {}
# Create an environment and start the setup process
env = simpy.Environment()
env.process(setup(env, NUM_ROOMS, CHANGETIME, T_INTER, PAYTIME, NUMCUSTS))

# Execute!
env.run(until = SIM_TIME)

print("\n")

for k, v in sorted(shopData.items(), key = lambda x : x[1][1]):
    print("%s had a priority of %i and shopped for %i minutes" % (k, v[1], v[2]))

print("\n")

for k, v in sorted(shopData.items(), key = lambda x : x[1][0]):
    print("%s had a priority of %i and shopped for %i minutes" % (k, v[1], v[2]))



'''
    random.seed(RANDOM_SEED)
    global env
    env = simpy.Environment()
    store = Store(env, NUM_ROOMS, CHANGETIME, PAYTIME)
    
    # Create 4 initial customers
    for i in range(10):
        c = Customer(env, "Customer %d " % i)
        print("Priority of %s is %i" % (c.custName, c.priority))
        env.process(c.shop(store))
    
    
    i = 3
    while i < 100:
        #
        #yield env.timeout(random.randint(T_INTER - 2, T_INTER + 2))
        i += 1
        c = Customer(env, "Customer %d " % i)
        env.process(c.shop(store))
    
    env.run(until = SIM_TIME)
    
if __name__ == "__main__":
    main()

'''    

