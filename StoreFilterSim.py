import simpy


class Shirt(object):
    def __init__(self, env, id):
        self.env = env
        self.name = 'shirt'
        self.id = id

class Jeans(object):
    def __init__(self, env, jeans_id):
        self.env = env
        self.name = 'jeans'
        self.id = jeans_id

class Customer(object):
    def __init__(self):
        self.preference = 1

def setup(env, arg):
    cust = Customer()
    for i in range(5):
        shirt = Shirt(env, i)
        jeans = Jeans(env, i)
        yield arg.put(shirt)
        yield arg.put(jeans)
    acc = 0
    flag = True
    while True:
        #print(acc)
        yield env.timeout(1)
        if flag:
            s = yield arg.get(filter = lambda s: isinstance(s, Shirt) and s.id == 2)
            flag = False
            print("Found shirt %i" % s.id)
        j = yield arg.get(filter = lambda y: isinstance(y, Jeans))
        print("Found jeans %i" % j.id)
        acc += 1
        

env = simpy.Environment()
res = simpy.FilterStore(env, capacity=50)
env.process(setup(env, res))
env.run(until=50)
